#!/bin/bash

# Step 1: Build Docker images
echo "Building Docker images..."
docker-compose build

# Step 2: Run containers in detached mode
echo "Starting containers..."
docker-compose up -d

# Step 3: Wait for services to be fully up and running
echo "Waiting for containers to start up..."
sleep 10

# Step 4: Check the status of the containers
echo "Checking container statuses..."
docker-compose ps

# Step 5: Executing commands inside the 'manager' container
echo "Executing commands in the 'manager' container..."
docker-compose exec -T manager whoami
docker-compose exec -T manager bash -c "cat /proc/1/status | grep CapEff"
docker-compose exec -T manager ansible all -m ping
docker-compose exec -T manager ansible-galaxy install -r /ansible/requirements.yml 
docker-compose exec -T manager ansible-playbook /ansible/k8playbook.yml 

# Step 6: Retrieve logs from the 'manager' container
echo "Retrieving logs from 'manager' container..."
docker-compose logs manager

echo "Script execution completed."
