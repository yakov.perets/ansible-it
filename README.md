# Ansible-Docker CI/CD Integration

This project demonstrates a practical implementation of Ansible within Docker containers, orchestrated through Docker Compose and automated via GitLab CI/CD. The primary focus is on using Ansible to manage configurations across multiple Ubuntu containers, ensuring they installed and configured.

run localy by:
- sudo swapoff -a
- chmod +x local-cicd.sh
- ./local-cicd.sh

 project structure:
|____.gitlab-ci.yml
|____manager
| |____Dockerfile
| |____ansible
| | |____ansible.cfg
| | |____hosts
| | |____requirements.yml
| | |____myplaybook.yml
| | |____k8playbook.yml
|____README.md
|____docker-compose.yaml
|____local-cicd.sh
|____nodes
| |____Dockerfile

manager/Dockerfile:

   FROM geerlingguy/docker-ubuntu2004-ansible
# The image is geerlingguy/docker-ubuntu2004-ansible, which is a pre-configured Ubuntu 20.04 image with Ansible installed. This image is maintained by Jeff Geerling and is tailored for Ansible playbook and role testing.
   RUN apt-get update && \
      apt-get install -y software-properties-common
#  It then installs software-properties-common, which provides necessary scripts (like add-apt-repository) to manage additional software repositories.
   RUN add-apt-repository universe
# The universe repository contains community-maintained free and open-source software. Adding this repository expands the range of packages available for installation.
   RUN apt-get update
# This is necessary to ensure that the package cache is up-to-date with all entries from the universe repository.
   RUN apt-get install -y sshpass gnupg && \
# sshpass is a utility for running ssh with the mode of non-interactively performing password authentication. gnupg is the GNU Privacy Guard, a tool for secure communication and data storage
      apt-get -f install
#  it's a way to ensure that any missing dependencies from previous installations are resolved.
   RUN apt-get clean && rm -rf /var/lib/apt/lists/*
# Cleans up unnecessary files, including the removal of package lists in /var/lib/apt/lists/. This is often done in Docker images to reduce the layer size, ensuring that the image uses less disk space.
   RUN ansible-galaxy collection install community.general
# Uses Ansible Galaxy, Ansible's official hub for sharing Ansible content, to install the community.general collection. This collection includes a variety of Ansible modules and plugins maintained by the Ansible community, which are not necessarily included in the core Ansible distribution.
   COPY ./ansible /ansible
   RUN ansible-galaxy install -r /ansible/requirements.yml --force-with-deps
# This file lists dependencies for running your Ansible playbooks.
   WORKDIR /ansible
   CMD ["tail", "-f", "/dev/null"]
# which effectively causes the container to do nothing and wait indefinitely. This command is typically used in containers meant to stay up but remain idle by default  .   waiting for manual interactions such as running Ansible commands or playbooks via docker exec

nodes/Dockerfile

   FROM ubuntu:20.04
   RUN apt-get update && apt-get install -y openssh-server python3
   RUN ln -s /usr/bin/python3 /usr/bin/python
# Creates a symbolic link to make python3 executable as python. This is useful because some older scripts or tools might call python instead of python3, and by default, Ubuntu 20.04 does not include a python executable.
   RUN mkdir /var/run/sshd
# Creates the directory /var/run/sshd, which is necessary for the operation of the SSH server. This directory is used by SSHD to store runtime information.
   RUN echo 'root:password' | chpasswd
# Sets the root password to "password". This allows logging in as root via SSH
   RUN sed -i 's/#PermitRootLogin prohibit-password/PermitRootLogin yes/' /etc/ssh/sshd_config
# Modifies the SSH server configuration to allow root login. The default setting is to disallow root login with a password.
   RUN sed -i 's@session\s*required\s*pam_loginuid.so@session optional pam_loginuid.so@g' /etc/pam.d/sshd
# Changes the PAM configuration for SSHD to make the pam_loginuid.so module optional. This adjustment can help avoid issues in certain containerized environments where PAM might prevent successful SSH logins due to how sessions are handled
   EXPOSE 22
# Port 22 is the standard port for SSH connections.
   CMD ["/usr/sbin/sshd", "-D"]
# This command runs the SSH server in the foreground, preventing the container from exiting. The -D option ensures that the SSH daemon does not daemonize (does not become a background process), which is suitable for container environments where you usually want the main process to run in the foreground.


docker-compose, where the communication are happenning:
   version: '3.8'
   services:
   manager:
      build:
         context: ./manager 
         dockerfile: Dockerfile
      container_name: manager
      environment:
         - ANSIBLE_HOST_KEY_CHECKING=False
      privileged: true
      networks:
         - ansible_network
   node1:
      build:
         context: ./nodes  
         dockerfile: Dockerfile
      container_name: node1
      networks:
         - ansible_network
   node2:
      build:
         context: ./nodes  
         dockerfile: Dockerfile
      container_name: node2
      networks:
         - ansible_network
   networks:
   ansible_network:
      driver: bridge

.gitlab-ci.yml     or local ./local-cicd.sh
build-deploy-test:
  stage: build-deploy-test
  script:
    - docker-compose build
    - docker-compose up -d 
    - sleep 10
    - docker-compose ps
    - docker-compose exec -T manager ansible all -m ping
    - docker-compose exec -T manager ansible-galaxy install -r /ansible/requirements.yml 
    - docker-compose exec -T manager ansible-playbook /ansible/k8playbook.yml 
    - docker-compose logs manager

manager/ansible/hosts:
[all]
node1 ansible_host=node1 ansible_connection=ssh ansible_ssh_user=root ansible_ssh_pass=password
node2 ansible_host=node2 ansible_connection=ssh ansible_ssh_user=root ansible_ssh_pass=password
node3 ansible_host=node3 ansible_connection=ssh ansible_ssh_user=root ansible_ssh_pass=password

manager/ansible/ansible.cfg:
[defaults]
inventory = /ansible/hosts
host_key_checking = False
remote_user = root
deprecation_warnings = False
command_warnings = False

[privilege_escalation]
become = True
become_method = sudo
become_user = root
become_ask_pass = False

manager/ansible/requirements.yml:
collections:
  - name: community.general

 now my wish is to create kubernetes cluster on this formation: 
1. by Use Ansible to ensure that all nodes are prepared for Kubernetes installation. This includes:
   - Installing Docker or another container runtime.
   - Ensuring the br_netfilter module is loaded and the required sysctl params are set.
   Turning off Swap.
   - Installing kubeadm, kubelet, and kubectl.
   - Initialize the Cluster on the Master Node: Use kubeadm init on one of your nodes to make it the master node. This will provide you with a kubeadm join command including a token and a CA cert hash, which you will use to join worker nodes to the cluster.
2. Join Worker Nodes to the Cluster: Using the kubeadm join command obtained from the initialization of the master node, join your worker nodes to the cluster.
3. Post-Installation Setup: Apply a network plugin like Calico, Weave, or Flannel to manage the networking between pods.
4. Verification: Ensure your cluster is up and running by executing kubectl get nodes and kubectl get pods --all-namespaces.

so already i have: some tasks on my k8playbook.yml: 
---
- hosts: all
  become: yes
  tasks:
    # System Preparation
    - name: Install essential system packages
   # Checking which user is running the playbook
    - name: Check which user is running the playbook
    - name: Debug - Print which user is running the playbook
    # Conditional Operations based on User and OS
    - name: Allow root user to execute sudo without password
    - name: Set up the Docker repository for Debian-based systems
    - name: Install Docker and related packages for Debian-based systems
    # System Configuration
    - name: Disable Swap and comment swap in fstab
    # Kubernetes Setup
    - name: Set up Kubernetes repository and install packages
        - name: Add Kubernetes GPG key
        - name: Add Kubernetes repository
        - name: Update apt cache and install Kubernetes packages
          loop:
            - kubeadm=1.28.1-1.1
            - kubelet=1.28.1-1.1
            - kubectl=1.28.1-1.1
    # Debugging and Verification
    - name: Retrieve Kubernetes repository list
    - name: Debug - Print Kubernetes repository list
    - name: Manually update apt cache and show errors
    - name: Output apt update result
until now you anderstand?: 


